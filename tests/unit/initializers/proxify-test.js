import Ember from 'ember';
import ProxifyInitializer from 'dummy/initializers/proxify';
import { module, test } from 'qunit';
import {isProxy} from 'ember-cli-proxify/proxy';

let application;

module('Unit | Initializer | proxify', {
  beforeEach() {
    Ember.run(function() {
      application = Ember.Application.create();
      application.deferReadiness();

      ProxifyInitializer.initialize(application);
    });
  }
});

test('new object functions are proxified', function(assert) {
	assert.expect(1);
	let obj = Ember.Object.extend({
		test() {
			assert.ok(isProxy(this), 'new object is proxy');
		}
	}).create();

	obj.test();
});