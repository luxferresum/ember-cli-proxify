import { module, test } from 'qunit';
import {proxify/*, isProxy*/} from 'ember-cli-proxify/proxy';
import Ember from 'ember';

const {get} = Ember;
// import sinon from 'sinon';
// import autosave from 'ember-autosave';
// import { module, test } from 'qunit';
// const { set } = Ember;

// var model;
// var clock;

const Demo = Ember.Object.extend({
  vals: {},
  unknownProperty(key) {
    return get(this, 'vals')[key];
  },
  setUnknownProperty(key, val) {
    get(this, 'vals')[key] = val;
  }
});

module('proxify function');

test('this is proxified', function(assert) {
  let obj = Demo.create({
    func() {
      this.foo = "baz";
    }
  });
  
  let proxy = proxify(obj);

  proxy.func();
  assert.equal(get(obj, 'vals').foo, 'baz');
});

test('arguments get proxified', function(assert) {
  let func = function(arg) {
    arg.baz = "bar";
  };

  let obj = Demo.create({});
  let proxy = proxify(func);
  proxy(obj);

  assert.equal(get(obj, 'vals').baz, 'bar');
});