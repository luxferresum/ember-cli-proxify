import { module, test } from 'qunit';
import {proxify, isProxy} from 'ember-cli-proxify/proxy';
import Ember from 'ember';

const {get} = Ember;
// import sinon from 'sinon';
// import autosave from 'ember-autosave';
// import { module, test } from 'qunit';
// const { set } = Ember;

// var model;
// var clock;

const Demo = Ember.Object.extend({
	foo: Ember.computed({
		get() {
			return "bar";
		}
	}),
	foo2: Ember.computed.alias('_foo2'),
});

module('using proxify');

test('getting with dot', function(assert) {
  let obj = Demo.create({});
  let proxy = proxify(obj);
  assert.equal(proxy.foo, "bar");
});

test('setting with dot', function(assert) {
  let obj = Demo.create({});
  let proxy = proxify(obj);
  proxy.foo2 = "baz";
  assert.equal(get(obj, '_foo2'), "baz");
});

test('return proxy on get so resolve chain', function(assert) {
	let obj = Demo.create({
		child: Demo.create({
			_foo2: 'foobaz',
		}),
	});
	let proxy = proxify(obj);
	assert.equal(proxy.child.foo2, 'foobaz');
});

test('return proxy on get to resolve chain for set', function(assert) {
	let obj = Demo.create({
		child: Demo.create(),
	});
	let proxy = proxify(obj);
	proxy.child.foo2 = "foobar";
	assert.equal(get(obj, 'child._foo2'), 'foobar');
});

test('unproxify if setting a proxy', function(assert) {
	let obj1 = Demo.create({});
	let obj2 = Demo.create({});
	let proxy1 = proxify(obj1);
	let proxy2 = proxify(obj2);
	proxy1.foobar = proxy2;
	assert.ok(!isProxy(get(obj1, 'foobar')));
});

test('can execute functions', function(assert) {
	let obj = Demo.create({
		foo: 'baz',
		func() {
			return this.get('foo');
		}
	});
	let proxy = proxify(obj);
	assert.ok(proxy.func(), 'baz');
});