# Ember-cli-proxify

Simple addon that allows to *wrap* Ember Objects so that `.get()` and `.set()` is not longer required.