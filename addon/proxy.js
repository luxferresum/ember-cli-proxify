import Ember from 'ember';

const {get,set} = Ember;

const $isProxy = Symbol();
const $target = Symbol();

const handler = {
	has() {
	},
	get(target, property/*, receiver*/) {
		if(property === $isProxy) {
			return true;
		}

		if(property === $target) {
			return target;
		}

		const value = get(target, property);
		if(typeof value !== 'function' && value instanceof Object) {
			return proxify(value);
		}

		return value;
	},
	set(target, property, value/*, receiver*/) {
		let val = isProxy(value) && value[$target] || value;
		set(target, property, val);
	},
};

function proxifyFunction(func) {
	return function proxyWrapper(...args) {
		func.apply(proxify(this), args.map(arg => proxify(arg)));
	};
}

export function proxify(target) {
	// if(arguments.length === 0 && typeof this === 'function') {
	// 	return proxifyFunction(this);
	// }

	if(typeof target === 'function') {
		return proxifyFunction(target);
	}

	if(target instanceof Object && !isProxy(target)) {
		return new Proxy(target, handler);
	}

	return target;
}

export function isProxy(target) {
	return target[$isProxy] || false;
}