import Ember from 'ember';
import {proxify} from '../proxy';

export function initialize(/* application */) {
  // application.inject('route', 'foo', 'service:foo');
  console.log('initialize');
  Ember.Object.reopenClass({
  	extend(...args) {
  		let last = args.pop();

  		Object.keys(last).forEach(key => {
  			last[key] = proxify(last[key]);
  		});

  		return this._super(...args, last);
  	}
  });
}

export default {
  name: 'proxify',
  initialize
};
